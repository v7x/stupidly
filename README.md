# Website Dot Com
*Source code for my personal website*

Written in CHICKEN Scheme using the [Awful](http://wiki.call-cc.org/eggref/5/awful) framework.

## LICENSES 
*  [Website Dot Com](https://gitlab.com/v7x/website-dot-com): [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
*  [Awful, Awful-SSL, Awful-Sqlite3](wiki.call-cc.org/eggref/5/awful): [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause)
*  [utf8](http://wiki.call-cc.org/eggref/5/utf8): [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause)
*  [lowdown](http://wiki.call-cc.org/eggref/5/lowdown): [BSD 3-Clause](https://opensource.org/licenses/BSD-3-Clause)
