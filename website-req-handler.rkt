#lang web-server

#|website-dot-com - Code for my personal website
Copyright (C) 2019 Andrew Mack

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
|#

(require web-server/http)
(require "website-page.rkt")

(define home-page
 (lambda (output)
  (display (make-page "home") output)))

(define-values (page-dispatch page-url)
 (dispatch-rules
  [(" ") home-page]
  [("faq") get-faq-page]
  [("about") get-about-page]
  [("map") get-site-map]
  [("projects") get-project-page]
  [("user" (string-arg)) get-user]
  [("article" (string-arg)) get-article]))
