CREATE TABLE IF NOT EXISTS account(
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    salt BLOB NOT NULL
);

CREATE TABLE IF NOT EXISTS auth(
    id INTEGER,
    hash BLOB NOT NULL,
    FOREIGN KEY(id) REFERENCES account(id)
);

CREATE TABLE IF NOT EXISTS gpg(
    id INTEGER,
    pubkey BLOB NOT NULL,
    privkey BLOB NOT NULL,
    FOREIGN KEY(id) REFERENCES account(id)
);

CREATE TABLE IF NOT EXISTS article(
    id INTEGER,
    title TEXT,
    author INTEGER,
    tstamp TEXT DEFAULT CURRENT_TIMESTAMP,
    content TEXT NOT NULL,
    PRIMARY KEY(id, title),
    FOREIGN KEY(author) REFERENCES account(id)
);
