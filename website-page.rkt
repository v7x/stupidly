#lang web-server

#|website-dot-com - Code for my personal website
Copyright (C) 2019 Andrew Mack

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
|#

(require web-server/templates xml)

(provide make-page)

(define base "base")

(define base-head
 `((meta ((name="viewport") (content="width=device-width, intial-scale=1.0")))
   (link ((rel="stylesheet") (type="text/css") (href="css/base.css")))
   (link ((rel="stylesheet") (type="text/css") (href="css/grid.css")))))

;note this is html header, not define-page argument 'headers' (html head)
(define base-header
 `(header ((class ,base) (id "main-header"))
   (h1 ((class ,base) (id "header-h1")))))

(define base-nav
 `(nav ((class ,base) (id "main-nav"))
   (table ((class ,base) (id "nav-table"))
    (tr ((class ,base) (id "nav-tr"))
     (th ((class ,base) (id "nav-th")(colspan "6"))))
    (tr ((class ,base) (id "nav-tr"))
     (td ((class ,base) (id "nav-td")) "Home")
     (td ((class base) (id "nav-td")) "About")
     (td ((class ,base) (id "nav-td")) "FAQ")
     (td ((class ,base) (id "nav-td")) "Blog")
     (td ((class ,base) (id "nav-td")) "Projects")
     (td ((class ,base) (id "nav-td")) "SiteMap")))))

(define base-aside
 `(aside ((class ,base) (id "main-aside"))))

(define (base-main content)
 `(main ((class ,base) (id "main-main")) ,@content))

(define base-footer
 `(footer ((class ,base) (id "main-footer"))))

(define (base-template title
                       main
                       #:head (head base-head)
                       #:header (header base-header)
                       #:nav (nav base-nav)
                       #:aside (aside base-aside)
                       #:footer (footer base-footer))
         (include-template "templates/base.html"))

(define-syntax html->str
 (syntax-rules ()
  ((html->str x y)
   (define (x) 
    (if (not (string? y))
     ((xexpr->str y))
     y)))))

(define (make-page path
                   main-content
                   #:template (template base-template)
                   #:head (head base-head)
                   #:header (header base-header)
                   #:nav (nav base-nav)
                   #:aside (aside base-aside)
                   #:main (main (base-main main-content))
                   #:footer (footer base-footer))
         (html->str head-str head)
         (html->str header-str header)
         (html->str nav-str nav)
         (html->str aside-str aside)
         (html->str main-str main)
         (html->str footer->str footer)
         (define page (base-template path
                                     main-str
                                     #:head head-str
                                     #:header header-str
                                     #:nav nav-str
                                     #:aside aside-str
                                     #:footer footer-str)))
